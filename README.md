# Pipeline-Example-Needs

There’s Job A and Job B and they want Job B to always execute when Job A executes but also if another part changes in the path of Job B (Job B depends on Job A but it should also be rebuilt when Job B’s own code/files change and not only when the dependent builds)

![Screenshot_2023-03-21_at_10.36.16](/uploads/2fa3f57d45ac4a2862cc563d6c89633d/Screenshot_2023-03-21_at_10.36.16.png)

## Run a Job when code changes

[Skip job if the branch is empty](https://docs.gitlab.com/ee/ci/jobs/job_control.html#skip-job-if-the-branch-is-empty)

```yaml
job:
  script:
    - echo "This job only runs for branches that are not empty"
  rules:
    - if: $CI_COMMIT_BRANCH
      changes:
        compare_to: 'refs/heads/main'
        paths:
          - '*'
```
